#!/usr/bin/env sh

# See https://stackoverflow.com/questions/59551850/remove-git-lfs-with-filter-branch-cant-re-add-unchanged-file-to-index

git filter-branch -f \
--prune-empty \
--tree-filter 'git rm -f --ignore-unmatch .gitattributes "matrix-sdk-android/src/androidTest/assets/*.realm"' \
--tag-name-filter cat -- \
--all 
